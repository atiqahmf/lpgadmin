<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::match(['get', 'post'], '/admin','AdminController@login');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::group(['middleware' => ['auth']],function(){
	Route::get('/admin/dashboard','AdminController@dashboard');	
	Route::get('/admin/settings','AdminController@settings');
	Route::get('/admin/check-pwd','AdminController@chkPassword');
	Route::match(['get','post'],'/admin/update-pwd','AdminController@updatePassword');

	// Dealers Routes (Admin)
	Route::get('/admin/view-dealers','DealerController@viewDealers');
	Route::match(['get','post'],'/admin/add-dealer','DealerController@addDealer');
	Route::match(['get','post'],'/admin/edit-dealer/{id}','DealerController@editDealer');
	Route::match(['get','post'],'/admin/delete-dealer/{id}','DealerController@deleteDealer');

	// Products Routes
	Route::get('/admin/view-products','ProductsController@viewProducts');
	Route::match(['get','post'],'/admin/add-product','ProductsController@addProduct');
	Route::match(['get','post'],'/admin/edit-product/{id}','ProductsController@editProduct');
	Route::match(['get','post'],'/admin/delete-product/{id}','ProductsController@deleteProduct');

	// Orders Routes
	Route::get('/admin/view-orders','OrdersController@viewOrders');
	Route::match(['get','post'],'/admin/edit-order/{id}','OrdersController@editOrder');
	Route::match(['get','post'],'/admin/delete-order/{id}','OrdersController@deleteOrder');
// });


Route::get('/logout','AdminController@logout');


