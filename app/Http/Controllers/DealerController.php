<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dealer;
use Illuminate\Support\Facades\DB;

class DealerController extends Controller
{
    public function addDealer(Request $request){
    	if($request->isMethod('post')){
    		$data = $request->all();
    		//echo "<pre>"; print_r($data); die;
    		$dealer = new Dealer;
    		$dealer->contact_name = $data['contact_name'];
            // $dealer->uuid = $data['uuid'];
    		$dealer->company = $data['company'];
            $dealer->contact_no = $data['contact_no'];
            $dealer->account_no = $data['account_no'];
            $dealer->address = $data['address'];
            $dealer->poscode = $data['poscode'];
            $dealer->town = $data['town'];
            $dealer->state = $data['state'];
            $dealer->region = $dealer->state;
    		$dealer->active = 1;
    		$dealer->save();
    		return redirect('/admin/view-dealers')->with('flash_message_success','Dealer added Successfully!');
    	}

        $levels = Dealer::where(['uuid'=>0])->get();

    	return view('admin.dealers.add_dealer')->with(compact('levels'));
    }

    public function editDealer(Request $request, $id = null){
        if($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            Dealer::where(['id'=>$id])->update(['contact_name'=>$data['contact_name'],'company'=>$data['company'],'contact_no'=>$data['contact_no']]);
            return redirect('/admin/view-dealers')->with('flash_message_success','Dealer updated Successfully!');
        }
        $dealerDetails = Dealer::where(['id'=>$id])->first();
        $levels = Dealer::where(['uuid'=>0])->get();
        return view('admin.dealers.edit_dealer')->with(compact('dealerDetails','levels'));
    }

    public function deleteDealer(Request $request, $id = null){
        if(!empty($id)){
            Dealer::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','Dealer deleted Successfully!');
        }
    }

    public function viewDealers(){

    	// $dealers = Dealer::get();

        $dealers = DB::table('dealer')
            ->where('active', '=', '1')
            ->get();
    	$dealers = json_decode(json_encode($dealers));
    	/*echo "<pre>"; print_r($dealers); die;*/
    	return view('admin.dealers.view_dealers')->with(compact('dealers'));
    }
}