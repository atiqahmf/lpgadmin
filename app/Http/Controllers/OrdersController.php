<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    public function addOrder(Request $request){
    	if($request->isMethod('post')){
    		$data = $request->all();
    		//echo "<pre>"; print_r($data); die;
    		$order = new Order;
    		$order->contact_name = $data['contact_name'];
            $order->uuid = $data['uuid'];
    		$order->company = $data['company'];
    		$order->contact_no = $data['contact_no'];
    		$order->save();
    		return redirect('/admin/view-orders')->with('flash_message_success','Order added Successfully!');
    	}

        $levels = Order::where(['uuid'=>0])->get();

    	return view('admin.orders.add_order')->with(compact('levels'));
    }

    public function editOrder(Request $request, $id = null){

        // echo "<pre>"; print_r($id); die;

        if($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            // Order::where(['id'=>$id])->update(['name'=>$data['name'],/*'address'=>$data['address'],*/'delivery_date'=>$data['delivery_date']]);

            DB::table('order')
            ->join('user', 'user.uuid', '=', 'order.user_id')
            ->join('address', 'order.address_id', '=', 'address.uuid')
            ->where(['order.id' => $id])
            ->update(['user.name' => $data['name'], 'order.delivery_date' => $data['delivery_date']]);
            // ->update(['user.name' => DB::raw('"'.$data['name'].'"'), 'order.delivery_date' => DB::raw('"'.$data['delivery_date'].'"') ]);

            // echo Order::where('status', 1)->toSql();

            return redirect('/admin/view-orders')->with('flash_message_success','Order updated successfully!');

            /* 
            DB::table('attributes as a')
           ->join('catalog as c', 'a.parent_id', '=', 'c.id')
           ->update([ 'a.key' => DB::raw("`c`.`left_key`") ]);
            */
        }
        // $orderDetails = Order::where(['id'=>$id])->first();

        $orderDetails = DB::table('order')
            ->select('order.*', 'user.name', 'address.*', 'dealer.*', 'order.id as order_id')
            ->join('user', 'user.uuid', '=', 'order.user_id')
            ->join('address', 'order.address_id', '=', 'address.uuid')
            ->join('dealer', 'order.dealer_id', '=', 'dealer.uuid')
            ->where(['order.id'=>$id])->first();
            // ->get();
            // ->where('order.id', '=', $id)
            // ->get();

            // echo "<pre>"; print_r($orderDetails); die;

        $levels = Order::where(['uuid'=>0])->get();
        return view('admin.orders.edit_order')->with(compact('orderDetails','levels'));
    }

    public function deleteOrder(Request $request, $id = null){
        if(!empty($id)){
            // Order::where(['id'=>$id])->delete();
            // return redirect()->back()->with('flash_message_success','Order deleted Successfully!');

            Order::where(['id'=>$id])->update(['deleted_at'=>now()]);

            return redirect('/admin/view-orders')->with('flash_message_success','Order updated successfully!');
        }
    }

    public function viewOrders(){

        /*
        SELECT *
        FROM `order`
        LEFT JOIN user ON user.uuid = `order`.user_id
        LEFT JOIN address ON `order`.`address_id`= address.uuid
        */

    	// $orders = Order::get();

        $orders = DB::table('order')
            ->select('order.*', 'user.name', 'address.*', 'dealer.*', 'order.id as order_id')
            ->join('user', 'user.uuid', '=', 'order.user_id')
            ->join('address', 'order.address_id', '=', 'address.uuid')
            ->join('dealer', 'order.dealer_id', '=', 'dealer.uuid')
            ->where(['order.deleted_at'=>NULL])
            ->get();

    	$orders = json_decode(json_encode($orders));
    	// echo "<pre>"; print_r($orders); die;
    	return view('admin.orders.view_orders')->with(compact('orders'));
    }
}