@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Orders</a> <a href="#" class="current">Edit Order</a> </div>
    <h1>Orders</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Edit Order</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/edit-order/'.$orderDetails->order_id) }}" name="edit_order" id="edit_order" novalidate="novalidate"> {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Order No</label>
                <div class="controls">
                  <input type="text" name="order_no" id="order_no" value="{{ $orderDetails->order_no }}" readonly>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Customer Name</label>
                <div class="controls">
                  <textarea name="name" id="name">{{ $orderDetails->name }}</textarea>
                </div>
              </div>
              <!-- <div class="control-group">
                <label class="control-label">Address</label>
                <div class="controls">
                  <textarea type="text" name="address" id="address">{{ $orderDetails->unit.' '.$orderDetails->apartment.' '.$orderDetails->street.' '.$orderDetails->area.' '.$orderDetails->postcode.' '.$orderDetails->town.' '.$orderDetails->state }}</textarea>
                </div>
              </div> -->
              <div class="control-group">
                <label class="control-label">Address</label>
                <div class="controls">
                  <input type="text" name="unit" id="unit" value="{{ $orderDetails->unit }}" placeholder="Unit No">
                </div>
                <div class="controls">
                  <input type="text" name="street" id="street" value="{{ $orderDetails->street }}" placeholder="Street">
                </div>
                <div class="controls">
                  <input type="text" name="area" id="area" value="{{ $orderDetails->area }}" placeholder="Area">
                </div>
                <div class="controls">
                  <input type="text" name="postcode" id="postcode" value="{{ $orderDetails->postcode }}" placeholder="Postcode">
                </div>
                <div class="controls">
                  <input type="text" name="town" id="town" value="{{ $orderDetails->town }}" placeholder="Town">
                </div>
                <div class="controls">
                  <input type="text" name="state" id="state" value="{{ $orderDetails->state }}" placeholder="State">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Dealer</label>
                <div class="controls">
                  <input type="text" name="company" id="company" value="{{ $orderDetails->company }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Delivery Date</label>
                <div class="controls">
                  <input type="text" name="delivery_date" id="delivery_date" value="{{ $orderDetails->delivery_date }}">
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Edit Order" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection