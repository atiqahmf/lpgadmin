@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="./dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Orders</a> <a href="#" class="current">View Orders</a> </div>
    <h1>Orders</h1>
     @if(Session::has('flash_message_error'))
        <div class="alert alert-error alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{!! session('flash_message_error') !!}</strong>
        </div>
    @endif   
    @if(Session::has('flash_message_success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{!! session('flash_message_success') !!}</strong>
        </div>
    @endif   
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>View Orders</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Order No</th>
                  <th>Customer Name</th>
                  <th>Customer Address</th>
                  <th>Dealer</th>
                  <th>Delivery Date</th>
                  <th>Order Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($orders as $order)
                <tr class="gradeX">
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $order->order_no }}</td>
                  <td>{{ $order->name }}</td>
                  <td>{{ $order->unit." ".$order->apartment." ".$order->street." ".$order->area." ".$order->postcode." ".$order->town." ".$order->state }}</td>
                  <td>{{ $order->company }}</td>
                  <td>{{ $order->delivery_date }}</td>

                  @if($order->status == 'new')
                    <td><span class="label label-success">{{ $order->status }}</span></td>
                  @else
                    <td><span class="label label-warning">{{ $order->status }}</span></td>
                  @endif
                  <!-- <td>{{ $order->status }}</td> -->
                  <td class="center"><a href="{{ url('/admin/edit-order/'.$order->order_id) }}" class="btn btn-primary btn-mini">Edit</a> <a id="delCat" href="{{ url('/admin/delete-order/'.$order->order_id) }}" class="btn btn-danger btn-mini">Delete</a></td>
                </tr>
                @endforeach
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection