@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="./dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Dealers</a> <a href="#" class="current">Add Dealer</a> </div>
    <h1>Dealers</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Add Dealer</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/add-dealer') }}" name="add_dealer" id="add_dealer" novalidate="novalidate"> {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Dealer Name</label>
                <div class="controls">
                  <input type="text" name="contact_name" id="contact_name" placeholder="John Doe">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Company</label>
                <div class="controls">
                  <input type="text" name="company" id="company" placeholder="PETRONAS Dagangan Berhad">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Contact No</label>
                <div class="controls">
                  <input type="text" name="contact_no" id="contact_no" placeholder="0123456789">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Account No</label>
                <div class="controls">
                  <input type="text" name="account_no" id="account_no" placeholder="90012345">
                </div>
              </div>
              <!-- <div class="control-group">
                <label class="control-label">Dealer Level</label>
                <div class="controls">
                  <select name="parent_id" style="width: 220px;">
                    <option value="0">Main Dealer</option>
                    @foreach($levels as $val)
                      <option value="{{ $val->id }}">{{ $val->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div> -->
              <!-- <div class="control-group">
                <label class="control-label">Address</label>
                <div class="controls">
                  <textarea name="address" id="address"></textarea>
                </div>
              </div> -->
              <div class="control-group">
                <label class="control-label">Address</label>
                <div class="controls">
                  <input type="text" name="address" id="address" placeholder="Address">
                </div>
                <div class="controls">
                  <input type="text" name="poscode" id="poscode" placeholder="Postcode">
                </div>
                <div class="controls">
                  <input type="text" name="town" id="town" placeholder="Town">
                </div>
                <div class="controls">
                  <input type="text" name="state" id="state" placeholder="State">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Postcode</label>
                <div class="controls">
                  <input type="text" name="poscode" id="poscode" placeholder="50088 ">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Town</label>
                <div class="controls">
                  <input type="text" name="town" id="town" placeholder="Kuala Lumpur City Centre">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">State</label>
                <div class="controls">
                  <input type="text" name="state" id="state" placeholder="Kuala Lumpur">
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Add Dealer" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection