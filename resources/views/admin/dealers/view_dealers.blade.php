@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="./dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Dealers</a> <a href="#" class="current">View Dealers</a> </div>
    <h1>Dealers</h1>
     @if(Session::has('flash_message_error'))
        <div class="alert alert-error alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{!! session('flash_message_error') !!}</strong>
        </div>
    @endif   
    @if(Session::has('flash_message_success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{!! session('flash_message_success') !!}</strong>
        </div>
    @endif   
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>View Dealers</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Dealer Name</th>
                  <th>Company</th>
                  <th>Contact No</th>
                  <th>Account No</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($dealers as $dealer)
                <tr class="gradeX">
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $dealer->contact_name }}</td>
                  <td>{{ $dealer->company }}</td>
                  <td>{{ $dealer->contact_no }}</td>
                  <td>{{ $dealer->account_no }}</td>
                  <td class="center"><a href="{{ url('/admin/edit-dealer/'.$dealer->id) }}" class="btn btn-primary btn-mini">Edit</a> <a id="delCat" href="{{ url('/admin/delete-dealer/'.$dealer->id) }}" class="btn btn-danger btn-mini">Delete</a></td>
                </tr>
                @endforeach
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection